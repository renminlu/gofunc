package cypher

import (
	"crypto/hmac"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
)

// hmac_sha1，返回长度40的字符数组
func HmacSha1(src, key []byte) []byte {
	m := hmac.New(sha1.New, key)
	m.Write(src)
	sum := m.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// hmac_sha1
func HmacSha1ToString(src, key string) string {
	m := hmac.New(sha1.New, []byte(key))
	m.Write([]byte(src))
	return hex.EncodeToString(m.Sum(nil))
}

// hmac_sha256，返回长度64的字符数组
func HmacSha256(src, key []byte) []byte {
	m := hmac.New(sha256.New, key)
	m.Write(src)
	sum := m.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// hmac_sha256
func HmacSha256ToString(src, key string) string {
	m := hmac.New(sha256.New, []byte(key))
	m.Write([]byte(src))
	return hex.EncodeToString(m.Sum(nil))
}

// hmac_sha512，返回长度128的字符数组
func HmacSha512(src, key []byte) []byte {
	m := hmac.New(sha512.New, key)
	m.Write(src)
	sum := m.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// hmac_sha512
func HmacSha512ToString(src, key string) string {
	m := hmac.New(sha512.New, []byte(key))
	m.Write([]byte(src))
	return hex.EncodeToString(m.Sum(nil))
}
