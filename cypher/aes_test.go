package cypher

/*
	测试单个文件：go test -v aes_test.go  aes.go
*/
import (
	// "net/url"
	// "strings"
	"testing"
)

func Test_aes(t *testing.T) {
	var iv = "12345678abcdefgh"          //服务端密码：初始向量
	var key = "123456789012345678901234" //用户端密码
	var text = "123456#$%^&*()😂"

	_, miwen := AESCBC_encrypt_base64(text, key, iv)
	// t.Log("密文", string(miwen))
	_, mw := AESCBC_decrypt_base64(miwen, key, iv)
	// t.Log(string(mw))
	if text != mw {
		t.Log("aes测试失败")
	}
}

//登录密钥【解密】
func Test_login(t *testing.T) {
	var keyiv = "678yuihjkbnm5tgb" //密码
	data := "h5FOtYPiNYN6YpKCI5fz7F5q5dCgHopkR9SanFKlL3OvpdbIx/qtOrIPiNVgTho8FucYbaMRtpdV5ZB27RRqzw=="
	_, plaintext222 := AESCBC_decrypt_base64(data, keyiv, keyiv)
	t.Log("登录密钥【解密】", plaintext222)
}
