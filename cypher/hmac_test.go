package cypher

/*
	测试单个文件：go test -v hmac_test.go  hmac.go
*/
import (
	"testing"
)

func Test_Hmac_Sha(t *testing.T) {
	var text = "12345678abcdefgh"
	var key = "12345678abcdefgh"

	text_hmac_sha1 := "b881fe21ab923ff05992470a4ac9a5bfa99768f0"
	text_hmac_sha256 := "e1a8e71056d992100c118ce197a9d7d0cebcd85e8624174a86008d94adc27e7e"
	text_hmac_sha512 := "8fa4e0bb6d10eab98094e27013061946318015e873f5ea4efd3cefccde8b4038daa52de27cb2e480326d9b721366c5e79978bbfd21247670b354d50eb2d399df"

	//hmac_sha1 --------------------------------------------------------------------------------------------------------
	//println(string(HmacSha1([]byte(text), []byte(key))))
	//println(len(HmacSha1([]byte(text), []byte(key))))
	//println(HmacSha1ToString(text, key))
	//println(len(HmacSha1ToString(text, key)))
	if text_hmac_sha1 != string(HmacSha1([]byte(text), []byte(key))) {
		t.Log("HmacSha1()测试失败")
	}
	if text_hmac_sha1 != HmacSha1ToString(text, key) {
		t.Log("HmacSha1ToString()测试失败")
	}

	//hmac_sha256 ------------------------------------------------------------------------------------------------------
	//println(string(HmacSha256([]byte(text), []byte(key))))
	//println(len(HmacSha256([]byte(text), []byte(key))))
	//println(HmacSha256ToString(text, key))
	//println(len(HmacSha256ToString(text, key)))
	if text_hmac_sha256 != string(HmacSha256([]byte(text), []byte(key))) {
		t.Log("HmacSha256()测试失败")
	}
	if text_hmac_sha256 != HmacSha256ToString(text, key) {
		t.Log("HmacSha256ToString()测试失败")
	}

	//hmac_sha512 ------------------------------------------------------------------------------------------------------
	//println(string(HmacSha512([]byte(text), []byte(key))))
	//println(len(HmacSha512([]byte(text), []byte(key))))
	//println(HmacSha512ToString(text, key))
	//println(len(HmacSha512ToString(text, key)))
	if text_hmac_sha512 != string(HmacSha512([]byte(text), []byte(key))) {
		t.Log("HmacSha512()测试失败")
	}
	if text_hmac_sha512 != HmacSha512ToString(text, key) {
		t.Log("HmacSha512ToString()测试失败")
	}
}
