package cypher

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
)

// Sha1，返回长度40的字符数组
func Sha1(src []byte) []byte {
	h := sha1.New()
	h.Write(src)
	sum := h.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// Sha1
func Sha1ToString(src string) string {
	h := sha1.New()
	h.Write([]byte(src))
	return hex.EncodeToString(h.Sum(nil))
}

// sha256，返回长度64的字符数组
func Sha256(src []byte) []byte {
	h := sha256.New()
	h.Write(src)
	sum := h.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// sha256
func Sha256ToString(src string) string {
	h := sha256.New()
	h.Write([]byte(src))
	return hex.EncodeToString(h.Sum(nil))
}

// sha512，返回长度128的字符数组
func Sha512(src []byte) []byte {
	h := sha512.New()
	h.Write(src)
	sum := h.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

// sha512
func Sha512ToString(src string) string {
	h := sha512.New()
	h.Write([]byte(src))
	return hex.EncodeToString(h.Sum(nil))
}
