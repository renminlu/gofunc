package cypher

import (
	"crypto/md5"
	"encoding/hex"
)

//返回32位的md5校验和
func Md5ToString(src string) string {
	h := md5.New()
	h.Write([]byte(src))
	return hex.EncodeToString(h.Sum(nil))
	//写法二：return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}

//返回32位的md5校验和
func Md5(src []byte) []byte {
	h := md5.New()
	h.Write(src)
	sum := h.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)
	return dst
}

//返回16位的md5校验和
func Md5_16(src []byte) []byte {
	return Md5(src)[8:24]
}
