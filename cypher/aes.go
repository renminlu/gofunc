package cypher

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
)

/*
 AES加密数据块分组长度必须为128bit(byte[16])，
【初始向量iv】的长度：必须是128bit(byte[16])，即：16个字符
【密码】的长度：可以是128bit、192bit、256bit，即16、24、32中的任意一个。
*/

//AEC加密（CBC模式）（输出base64）
func AESCBC_encrypt_base64(plainText, key, iv string) (error, string) {
	err, c := AESCBC_encrypt([]byte(plainText), []byte(key), []byte(iv))
	if err != nil {
		return err, ""
	}
	return nil, base64.StdEncoding.EncodeToString(c)
}

//AEC解密（CBC模式）（输入base64）
func AESCBC_decrypt_base64(cipherText, key, iv string) (error, string) {
	oriCipher, err := base64.StdEncoding.DecodeString(cipherText)
	if err != nil {
		return err, ""
	}
	err, p := AESCBC_decrypt(oriCipher, []byte(key), []byte(iv))
	if err != nil {
		return err, ""
	}
	return nil, string(p)
}

//AEC加密（CBC模式）
func AESCBC_encrypt(plainText, key, iv []byte) (error, []byte) {
	//指定加密算法，返回一个AES算法的Block接口对象
	block, err := aes.NewCipher(key)
	if err != nil {
		return err, nil
	}
	//进行填充
	plainText = aAESCBC_padding(plainText, block.BlockSize())

	//指定分组模式，返回一个BlockMode接口对象
	blockMode := cipher.NewCBCEncrypter(block, iv)
	//加密连续数据库
	cipherText := make([]byte, len(plainText))
	blockMode.CryptBlocks(cipherText, plainText)
	//返回密文
	return nil, cipherText
}

//AEC解密（CBC模式）
func AESCBC_decrypt(cipherText, key, iv []byte) (error, []byte) {
	//指定解密算法，返回一个AES算法的Block接口对象
	block, err := aes.NewCipher(key)
	if err != nil {
		return err, nil
	}
	blockMode := cipher.NewCBCDecrypter(block, iv) //指定分组模式，返回一个BlockMode接口对象
	//解密
	plainText := make([]byte, len(cipherText))
	blockMode.CryptBlocks(plainText, cipherText)
	plainText = aAESCBC_unPadding(plainText) //删除填充
	return nil, plainText
}

//对明文进行填充
func aAESCBC_padding(plainText []byte, blockSize int) []byte {
	//计算要填充的长度
	n := blockSize - len(plainText)%blockSize
	//对原来的明文填充n个n
	temp := bytes.Repeat([]byte{byte(n)}, n)
	plainText = append(plainText, temp...)
	return plainText
}

//对密文删除填充
func aAESCBC_unPadding(cipherText []byte) []byte {
	//取出密文最后一个字节end
	end := cipherText[len(cipherText)-1]
	//删除填充
	cipherText = cipherText[:len(cipherText)-int(end)]
	return cipherText
}
