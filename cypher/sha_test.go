package cypher

/*
	测试单个文件：go test -v sha_test.go  sha.go
*/
import (
	"testing"
)

func Test_sha(t *testing.T) {
	var text = []byte("123456%^&*()VBJNKMbnj")
	text_1 := "eb491ed73e4897b36fd5442b8d93007f7ed3710c"
	text_256 := "1d2423a5e8d2332259e1aab12e9c962da769daeb79388429f673f694178857c3"
	text_512 := "99686a7c7cb42f53fa517af1da6df72bbc621fe7dc36f685b11eeb87ea88c16fd1ee1a5d5a845125ee72eed8133e47cae4a3b06d7dd3f3af23aedf3f2fe1e535"

	//t.Log(string(Sha1(text)))
	//t.Log(len(Sha1(text)))
	//t.Log(string(Sha256(text)))
	//t.Log(len(Sha256(text)))
	//t.Log(string(Sha512(text)))
	//t.Log(len(Sha512(text)))

	//sha1
	if text_1 != string(Sha1(text)) {
		t.Log("Sha1()失败")
	}
	if len(Sha1(text)) != 40 {
		t.Log("Sha1()长度不是40")
	}

	//sha256
	if text_256 != string(Sha256(text)) {
		t.Log("Sha256()失败")
	}
	if len(Sha256(text)) != 64 {
		t.Log("Sha256()长度不是64")
	}

	//sha512
	if text_512 != string(Sha512(text)) {
		t.Log("Sha512()失败")
	}
	if len(Sha512(text)) != 128 {
		t.Log("Sha512()长度不是128")
	}
}
