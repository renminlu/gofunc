package cypher

/*
	测试单个文件：go test -v md5_test.go  md5.go
*/
import (
	"testing"
)

func Test_md5(t *testing.T) {
	var text = []byte("123456")
	text_sum_32 := "e10adc3949ba59abbe56e057f20f883e"
	text_sum_16 := "49ba59abbe56e057"

	//t.Log(string(Md5(text)))
	//t.Log(string(Md5_16(text)))

	//32位
	if text_sum_32 != string(Md5(text)) {
		t.Log("Md5()失败")
	}
	if len(Md5(text)) != 32 {
		t.Log("Md5()返回值不是32位")
	}

	//16位
	if text_sum_16 != string(Md5_16(text)) {
		t.Log("Md5_16()失败")
	}
	if len(Md5_16(text)) != 16 {
		t.Log("Md5_16()返回值不是16位")
	}

}
