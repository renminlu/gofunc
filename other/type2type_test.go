package other

import (
	"testing"
)

func Test_String2int64(t *testing.T) {
	t.Log(String2int64("123"))   //123
	t.Log(String2int64("123aa")) //0
	t.Log(String2int64("abc"))   //0
	t.Log(String2int64("在这种"))   //0
	t.Log(String2int64("1d，，"))  //0
}

func Test_Other2string(t *testing.T) {
	t.Log(Other2string(123))   //123
	t.Log(Other2string("aaa")) //aaa
	t.Log(Other2string(false)) //false
	t.Log(Other2string(12.34)) //12.340000
	t.Log(Other2string('a'))   //97
}

func Test_Other2fieldVal(t *testing.T) {
	t.Log(Other2fieldVal(123))   //'123'
	t.Log(Other2fieldVal("aaa")) //'aaa'
	t.Log(Other2fieldVal(false)) //'false'
	t.Log(Other2fieldVal(12.34)) //'12.340000'
	t.Log(Other2fieldVal('a'))   //'97'
}
