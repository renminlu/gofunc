package other

import (
	"math/rand"
	"time"
)

// 随机生成指定位数的验证码
// @length 验证码长度
// @letter 是否含有字母，字母里剔除了易混淆的字母：I、O、Z
// 说明： 在一秒内调用多次，验证码仍然相同，但满足业务需要
func GetVerifyCode(length int, letter bool) string {
	base := []byte("0123456789")
	if letter {
		base = []byte("0123456789ABCDEFGHJKLMN0123456789PQRSTUVWXY0123456789")
	}

	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		result = append(result, base[r.Intn(len(base))])
	}
	return string(result)
}
