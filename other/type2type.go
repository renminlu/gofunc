package other

import (
	"fmt"
	"strconv"
)

// 字符串转int64
// 转换错误仍然返回0，不关注错误或认为错误等同于0
// bug：会把float转为0L
func String2int64(str string) int64 {
	i64, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return i64
}

// 字符串转int32
// 转换错误仍然返回0，不关注错误或认为错误等同于0
func String2int32(str string) int64 {
	i64, err := strconv.ParseInt(str, 10, 32)
	if err != nil {
		return 0
	}
	return i64
}

// 把其他基础数据类型转成字符串
// 匹配错误反馈空，不关注错误或认为错误等于空
func Other2string(i interface{}) (s string) {
	switch i.(type) {
	case bool:
		s = fmt.Sprintf("%t", i)
	case string:
		s = fmt.Sprintf("%s", i)
	case int, int32, int64:
		s = fmt.Sprintf("%d", i)
	case float32, float64:
		s = fmt.Sprintf("%f", i)
	case byte:
		s = fmt.Sprintf("%c", i)
	default:
		s = ""
	}
	return
}

//把其他基础数据类型转成适用于拼接原生sql的字段值（加单引号的字符串）
func Other2fieldVal(i interface{}) string {
	return "'" + Other2string(i) + "'"
}
