# 
### go语言功能集合包
	功能：function
	
# 
### 注意
	1、包与包之间尽量不要交叉感染，该冗余就冗余（只有相对，没有绝对）
	
# 
### 功能列表
| 包 | 功能 |
|--|--|
| cypher | 加密 |
| encode | 编码 |
| file | 目录、文件、内容操作 | 
| tslice | 数据类型：slice相关函数 |
| tstring | 数据类型：string相关函数 |