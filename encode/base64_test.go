package encode

/*
	测试单个文件：go test -v base64_test.go  base64.go
*/
import (
	"testing"
)

func Test_base64(t *testing.T) {
	text := "123456#$%^&*()😂"
	text_base64 := "MTIzNDU2IyQlXiYqKCnwn5iC"

	//编码
	encode := Base64EncodeToString(text)
	//println(encode)
	if text_base64 != encode {
		t.Log("Base64EncodeToString()测试失败")
	}
	encode = string(Base64Encode([]byte(text)))
	//println(encode)
	if text_base64 != encode {
		t.Log("Base64Encode()测试失败")
	}

	//解码
	decode, err := Base64DecodeToString(encode)
	//println(decode)
	if err != nil {
		t.Log("Base64DecodeToString()测试失败：" + err.Error())
	}
	if text != decode {
		t.Log("Base64DecodeToString()测试失败")
	}
	decode2, err2 := Base64Decode([]byte(encode))
	//println(string(decode2))
	if err2 != nil {
		t.Log("Base64Decode()测试失败：" + err2.Error())
	}
	if text != string(decode2) {
		t.Log("Base64Decode()测试失败")
	}
}
