package encode

import "encoding/base64"

// 编码
func Base64Encode(src []byte) []byte {
	b := base64.StdEncoding
	buf := make([]byte, b.EncodedLen(len(src)))
	b.Encode(buf, src)
	return buf
}
func Base64EncodeToString(src string) string {
	return string(base64.StdEncoding.EncodeToString([]byte(src)))
}

// 解码
// 解码失败，返回空字符串
func Base64Decode(src []byte) ([]byte, error) {
	base := base64.StdEncoding
	s := make([]byte, base.DecodedLen(len(src)))
	n, err := base.Decode(s, []byte(src))
	s = s[:n]
	return s, err
}
func Base64DecodeToString(src string) (string, error) {
	a, err := base64.StdEncoding.DecodeString(src)
	return string(a), err
}
