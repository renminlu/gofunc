package tstring

import (
	"regexp"
)

//密码
//8-20个大小写字母、数字、下划线；（不含特殊符号）
func Is_passwd(s string) bool {
	reg := `^(\w){8,20}$`
	rgx := regexp.MustCompile(reg)
	return rgx.MatchString(s)
}

//邮箱
func Is_email(email string) bool {
	//pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	pattern := `^[0-9a-z][_.0-9a-z-]{0,31}@([0-9a-z][0-9a-z-]{0,30}[0-9a-z]\.){1,4}[a-z]{2,4}$`

	reg := regexp.MustCompile(pattern)
	return reg.MatchString(email)
}

//手机号
func Is_mobile(mobile string) bool {
	regular := "^1\\d{10}$"

	reg := regexp.MustCompile(regular)
	return reg.MatchString(mobile)
}
