package tstring

/*
	转换
*/

import (
	"unicode"
)

//首字母转大写
func Change_firstToUpper(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

//首字母转小写
func Change_firstToLower(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

// 防止sql注入：把一段字符串中的特殊字符替换成html实体字符
//
// 主要防止sql注入，，如果是富文本，建议写入文件
// HTML转移字符大全 https://www.cnblogs.com/wjw6692353/p/11045417.html
func Change_HtmlEntity(s string) (r string) {
	r = strings.Replace(s, " ", "&ensp;", -1)
	r = strings.Replace(r, "<", "&lt;", -1)
	r = strings.Replace(r, ">", "&gt;", -1)
	r = strings.Replace(r, "=", "&#61;", -1)
	return
}
