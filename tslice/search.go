package tslice

import (
	"reflect"
)

//返回val在【切片】中：首次出现的索引位置
//适用于：任何数据类型
func Search(array interface{}, val interface{}) (index int) {
	index = -1
	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		{
			s := reflect.ValueOf(array)
			for i := 0; i < s.Len(); i++ {
				if reflect.DeepEqual(val, s.Index(i).Interface()) {
					index = i
					return
				}
			}
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于string
func Search_str(array []string, val string) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于int64
func Search_int(array []int64, val int64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于uint64类型
func Search_uint(array []uint64, val uint64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于bool
func Search_bool(array []bool, val bool) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于float64
func Search_float(array []float64, val float64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

//返回val在【切片】中：首次出现的索引位置
//仅适用于complex128
func Search_complex(array []complex128, val complex128) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}
