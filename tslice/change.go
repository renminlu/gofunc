package tslice

import (
	"strings"
)

//字符串类型切片：转：字符串
func Change_strSliceToStr(strSlice []string, splitStr string) string {
	var s string
	for _, v := range strSlice {
		s += v + splitStr
	}
	//去除最后一个splitStr
	s = strings.TrimRight(s, splitStr)
	return s
}
