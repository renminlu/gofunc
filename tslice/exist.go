package tslice

//判断：val是否存在【切片】中
//适用于：任何数据类型
func Exist(array interface{}, val interface{}) bool {

	if Search(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：string
func Exist_str(array []string, val string) bool {

	if Search_str(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：int
func Exist_int(array []int64, val int64) bool {

	if Search_int(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：uint
func Exist_uint(array []uint64, val uint64) bool {

	if Search_uint(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：bool
func Exist_bool(array []bool, val bool) bool {

	if Search_bool(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：float
func Exist_float(array []float64, val float64) bool {

	if Search_float(array, val) > -1 {
		return true
	}
	return false
}

//判断：val是否存在【切片】中
//仅适用于：Complex
func Exist_complex(array []complex128, val complex128) bool {

	if Search_complex(array, val) > -1 {
		return true
	}
	return false
}
