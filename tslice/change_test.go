package tslice

/*
	测试单个文件：
	go test -v change_test.go  change.go
*/
import (
	"fmt"
	"testing"
)

func Test_StrSliceToStr(t *testing.T) {
	slice := []string{"aaa", "bbb"}
	s := Change_StrSliceToStr(slice, "/")

	rightV := "aaa/bbb"
	if s != rightV {
		fmt.Println("StrSliceToStr错误! ", "正确值：", rightV, "；错误值：", s)
	}

	fmt.Println("StrSliceToStr正确，测试结果：", s)
}
