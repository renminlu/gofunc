package file

import (
	"testing"
)

//获取文件名
func Test_getFileName(t *testing.T) {
	var name string

	//绝对路径
	name = FileInfo_pureName("G:\\Project\\Work\\open\\open_go\\src\\yel\\main.go")
	t.Log(name)

	//相对路径
	name = FileInfo_pureName("yel\\main.go")
	t.Log(name)

	//文件名
	name = FileInfo_pureName("main.go")
	t.Log(name)

	//文件名
	name = FileInfo_pureName("main")
	t.Log(name)
}
