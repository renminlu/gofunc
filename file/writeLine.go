package file

import (
	"os"
	"sync"
)

var (
	//单列
	writeLineSingle map[string]*WriteLine = make(map[string]*WriteLine, 2)
	writeLineLock   sync.Mutex            //全局的互斥锁
)

//添加写（不是覆盖写）
type WriteLine struct {
	osFile *os.File
}

//创建一个写对象
//若文件不存在则创建
func NewWriteLine(pFilename string) (*WriteLine, error) {
	sw, ok := writeLineSingle[pFilename] //以文件名为键
	if ok {
		return sw, nil //【单列】如果存在则直接返回
	}
	w := WriteLine{}

	//【追加写】若文件不存在则创建
	fh, err := os.OpenFile(pFilename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		return &w, err
	}
	w.osFile = fh
	writeLineSingle[pFilename] = &w
	return &w, nil
}

//写
func (w *WriteLine) Write(str string) (n int, err error) {
	str += EOL
	writeLineLock.Lock() //加锁
	n, err = w.osFile.WriteString(str)
	writeLineLock.Unlock() //解锁
	return
}

//释放资源
func (w *WriteLine) Close() error {
	err := w.osFile.Close()
	return err
}
