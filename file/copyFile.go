package file

import (
	"io/ioutil"
)

//拷贝文件：适合小文件拷贝
func CopyFile(oldfile, newfile string) error {
	//读
	data, err := ioutil.ReadFile(oldfile)
	if err != nil {
		return err
	}
	//写
	err = ioutil.WriteFile(newfile, data, 0666)
	if err != nil {
		return err
	}
	return nil
}

//拷贝文件：适合大文件拷贝
// func CopyBigFile(oldfile, newfile string) error {
// 	return nil
// }
