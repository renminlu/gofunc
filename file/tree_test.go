package file

import (
	"testing"
)

var (
	//相对目录要加点
	test002 string = "./testData/TestTree/" //共4个文件
)

func TestTree(t *testing.T) {
	ls, errls, err := Tree(test002)

	if len(ls) != 5 {
		t.Fatalf("Tree(%v) 执行错误，len(ls)期望值=%v 实际值=%v\n", test002, 3, len(ls))
	}
	if len(errls) != 0 {
		t.Fatalf("Tree(%v) 执行错误,，len(errls)期望值=%v 实际值=%v\n", test002, 1, len(errls))
	}
	if err != nil {
		t.Fatalf("Tree(%v) 执行错误，err期望值=%v 实际值=%v\n", test002, "nil", err)
	}
	t.Log(ls)
}
func TestTreeFile(t *testing.T) {
	ls, errls, err := Tree_file(test002)

	if len(ls) != 4 {
		t.Fatalf("TreeFile(%v) 执行错误，len(ls)期望值=%v 实际值=%v\n", test002, 2, len(ls))
	}
	if len(errls) != 0 {
		t.Fatalf("TreeFile(%v) 执行错误,，len(errls)期望值=%v 实际值=%v\n", test002, 1, len(errls))
	}
	if err != nil {
		t.Fatalf("TreeFile(%v) 执行错误，err期望值=%v 实际值=%v\n", test002, "nil", err)
	}
	t.Log(ls)
}

func TestTreeDir(t *testing.T) {
	ls, errls, err := Tree_dir(test002)

	if len(ls) != 1 {
		t.Fatalf("TreeDir(%v) 执行错误，len(ls)期望值=%v 实际值=%v\n", test002, 1, len(ls))
	}
	if len(errls) != 0 {
		t.Fatalf("TreeDir(%v) 执行错误,，len(errls)期望值=%v 实际值=%v\n", test002, 1, len(errls))
	}
	if err != nil {
		t.Fatalf("TreeDir(%v) 执行错误，err期望值=%v 实际值=%v\n", test002, "nil", err)
	}
	t.Log(ls)
}
