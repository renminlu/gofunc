package file

import (
	"testing"
)

var (
	wf  string = "./testData/write/wf.txt"
	err error
)

func Test_WriteLine(t *testing.T) {
	var (
		write     *WriteLine
		writeLeng int
	)

	if write, err = NewWriteLine(wf); err != nil {
		t.Errorf("NewWriteLine错误：%v\n", err)
	}
	if writeLeng, err = write.Write("123你好"); err != nil {
		t.Errorf("NewWriteLine之Write错误：%v\n", err)
	}
	if writeLeng != 11 {
		t.Errorf("NewWriteLine之Write字符数量，应写=%v  实写=%v  \n", 11, writeLeng)
	}
	if err = write.Close(); err != nil {
		t.Errorf("NewWriteLine之Close错误：%v\n", err)
	}
}
