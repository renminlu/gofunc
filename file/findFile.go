package file

//搜索文件名
//搜索文件内容：见【filecontent没做】
import (
	"io/ioutil"
	"os"
	"regexp"
)

//
//功能：在当前目录查找文件
//说明：不递归子目录,返回值不包含路径
//
//参数：
// path	查询路径
// reg	匹配字符串，支持正则
func FindFile_inCurrentDir(path, reg string) (files []string, err error) {
	var sonList []os.FileInfo
	if sonList, err = ioutil.ReadDir(path); err != nil {
		return
	}
	var r *regexp.Regexp
	if r, err = regexp.Compile(reg); err != nil {
		return
	}
	for _, v := range sonList {
		//查找【子串】是否在指定的字符串中
		// strings.Contains(v.Name(), "foo") //true
		if r.MatchString(v.Name()) {
			files = append(files, v.Name())
		}
	}

	return
}

//递归搜索子孙文件列表
