package file

import (
	"os"
	"path/filepath"
)

//获取该目录及其该目录的子孙目录的列表
//不包含【非目录文件】
//路径：绝对路径
func Tree_dir(rootPath string) ([]string, []error, error) {
	var ls []string
	var errSlice []error
	err := filepath.Walk(rootPath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				errSlice = append(errSlice, err)
			}
			if f != nil && f.IsDir() {
				ls = append(ls, path)
			}
			return nil
		})
	return ls, errSlice, err
}

//  获取某个目录的【子孙文件】的路径信息列表
//	不包含【目录】
//  路径：绝对路径
//  调用示例
//  ls, _, _ := filepath.TreeFile("/findFile")
//  for _, v := range ls {
//  	fmt.Println(v)
//  }
func Tree_file(rootPath string) ([]string, []error, error) {
	var ls []string
	var errSlice []error
	err := filepath.Walk(rootPath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				errSlice = append(errSlice, err)
			}
			if f != nil && !f.IsDir() {
				ls = append(ls, path)
			}
			return nil
		})
	return ls, errSlice, err
}

//获取该目录及其该目录的子孙元素的列表
//说明：元素=目录+文件
//路径：绝对路径
func Tree(rootPath string) ([]string, []error, error) {
	var ls []string
	var errSlice []error
	err := filepath.Walk(rootPath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				errSlice = append(errSlice, err)
			}
			if f != nil {
				ls = append(ls, path)
			}
			return nil
		})

	return ls, errSlice, err
}
