package file

import (
	"testing"
)

//判断：是否是空目录
func Test_IsEmptyDir(t *testing.T) {
	t.Log(Is_emptyDir("./testData"))
	t.Log(Is_emptyDir("./testData/emptyDir"))
	t.Log(Is_emptyDir("./testData/123.u"))
	t.Log(Is_emptyDir("./testData1111"))
}
