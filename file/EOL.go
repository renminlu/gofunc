package file

// 换行符
import (
	"runtime"
)

var (
	EOL string //换行符
)

func init() {

	switch runtime.GOOS {
	case "windows":
		EOL = "\r\n"
	default:
		EOL = "\n"
	}
}
