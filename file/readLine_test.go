package file

import (
	"fmt"
	"testing"
)

var (
	ff string = "./testData/read/zhushi.txt"
)

func Test_readLine(t *testing.T) {
	var slice []string
	var err error

	if slice, err = ReadLine_trimPass(ff); err == nil {
		fmt.Println("----------------- ReadLineTrimPass ----------------------")
		for _, v := range slice {
			fmt.Println(v)
		}
	}

	if slice, err = ReadLine_trim(ff); err == nil {
		fmt.Println("----------------- ReadLineTrim --------------------------")
		for _, v := range slice {
			fmt.Println(v)
		}
	}

	if slice, err = ReadLine(ff); err == nil {
		fmt.Println("----------------- ReadLine ------------------------------")
		for _, v := range slice {
			fmt.Println(v)
		}
	}
}
