package file

import (
	"os"
	"path/filepath"
)

//参考TreeDir，忽略遍历中的文件错误
func TreeErrPass_dir(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f != nil {
				if f.IsDir() {
					ls = append(ls, path)
				}
			}
			return nil
		})
	return ls, err
}

//参考TreeFile，忽略遍历中的文件错误
func TreeErrPass_file(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f != nil {
				if !f.IsDir() {
					ls = append(ls, path)
				}
			}
			return nil
		})
	return ls, err
}

//参考Tree，忽略遍历中的文件错误
func TreeErrPass(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f != nil {
				ls = append(ls, path)
			}
			return nil
		})
	return ls, err
}
