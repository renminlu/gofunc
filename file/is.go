package file

import (
	"io/ioutil"
	"os"
)

//判断文件或目录：是否存在
//旧原型 func FileExist(path string) bool {
func Is_fileExist(path string) bool {

	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

//判断：是文件=true，是其他=false
func Is_file(f string) bool {
	fi, e := os.Stat(f)
	if e != nil {
		return false
	}
	return !fi.IsDir()
}

//判断：是目录=true，是其他=false
func Is_dir(f string) bool {

	fi, e := os.Stat(f)
	if e != nil {
		return false
	}
	return fi.IsDir()
}

//判断：目录是否为空；
//retrun：是空目录则返回true；不是目录或目录不为空则返回false
func Is_emptyDir(dir string) bool {
	list, err := ioutil.ReadDir(dir)
	//不是目录
	if err != nil {
		return false
	}
	//是空目录
	if len(list) == 0 {
		return true
	}
	//不是空目录
	return false
}
