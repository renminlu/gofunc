package file

import (
	"bufio"
	"os"
	"strings"
)

/*
 	一次读取：
  		读取整个文件，并返回字符串切片
 		适用于小文件
	多次读取：
		适用于大文件
		【待完善】
*/

//【去除“含注释#”的行】一次读取，存放到字符串切片中
//读取时：去除每行的开头和结尾的空格
func ReadLine_trimPass(filename string) (slice []string, err error) {
	var f *os.File
	if f, err = os.Open(filename); err != nil {
		return slice, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line) //去除两头空格
		//过滤空 || 过滤带注释的行
		if len(line) == 0 || strings.HasPrefix(line, "#") {
			continue
		}
		slice = append(slice, line)
	}
	err = scanner.Err()
	return
}

//一次读取，存放到字符串切片中
//读取时：去除每行的开头和结尾的空格，不处理#注释
func ReadLine_trim(filename string) (slice []string, err error) {
	var f *os.File
	if f, err = os.Open(filename); err != nil {
		return slice, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line) //去除两头空格
		//过滤空
		if len(line) == 0 {
			continue
		}
		slice = append(slice, line)
	}
	err = scanner.Err()
	return
}

//一次读取，存放到字符串切片中
//不处理原文
func ReadLine(filename string) (slice []string, err error) {
	var f *os.File
	if f, err = os.Open(filename); err != nil {
		return slice, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		slice = append(slice, line)
	}
	err = scanner.Err()
	return
}
