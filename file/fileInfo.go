package file

//获取文件信息

import (
	"os"
	"path"
	"path/filepath"
	"strings"
)

//根据文件名，获取修改时间，
//返回值：（秒级时间戳，error）
func FileInfo_uptime(file string) (int64, error) {

	f, err := os.Open(file)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return 0, err
	}

	times := fi.ModTime().Unix()
	return times, nil
}

//获取文件名(纯粹的名称，不含文件扩展名)
//GetFilePureName("G:\\Project\\Work\\main.go")
//GetFilePureName("Work\\main.go")
//GetFilePureName("main.go")
//GetFilePureName("main")
func FileInfo_pureName(pf string) (fileName string) {

	base := filepath.Base(pf) //获取路径中的文件名test.txt
	ext := path.Ext(pf)       //获取路径中的文件的后缀 .txt

	//去掉字符串【右边】的指定的字符
	fileName = strings.TrimRight(base, ext)
	return
}
