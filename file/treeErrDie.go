package file

import (
	"os"
	"path/filepath"
)

//参考TreeDir，遇到文件错误就停止遍历，并返回已经遍历到的文件和错误信息
func TreeDirErr_die(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err //会退出循环遍历
			}
			if f.IsDir() {
				ls = append(ls, path)
			}

			return nil
		})
	return ls, err
}

//参考TreeFile，遇到文件错误就停止遍历，并返回已经遍历到的文件和错误信息
func TreeFileErr_die(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err //会退出循环遍历
			}

			if !f.IsDir() {
				ls = append(ls, path)
			}
			return nil
		})
	return ls, err
}

//参考Tree，遇到文件错误就停止遍历，并返回已经遍历到的文件和错误信息
func TreeErrDie(dirpath string) ([]string, error) {
	var ls []string
	err := filepath.Walk(
		dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err //会退出循环遍历
			}
			ls = append(ls, path)
			return nil
		})
	return ls, err
}
