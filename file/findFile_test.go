package file

import (
	"testing"
)

var (
	test001 string = "./testData/" //相对目录要加点
)

//在当前目录查找文件
func Test_FindFilesInCurrentDir(t *testing.T) {
	var ls []string

	ls, _ = FindFile_inCurrentDir(test001, "123")
	t.Log(ls)

	ls, _ = FindFile_inCurrentDir(test001, ".d")
	t.Log(ls)
}
